FROM mariadb:10.3.17

# Metadata
LABEL maintainer="laemmi@spacerabbit.de"

# Packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    ssh \
    rsync

WORKDIR /app

RUN mkdir /app/tmp